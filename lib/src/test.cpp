#include "test.h"
#include <iostream>
#include <thread>
#include <chrono>

void crash() { volatile int* a = (int*)(NULL); *a = 1; }

void Test::foo()
{
    std::thread crashing_thread(crash);

	std::this_thread::sleep_for(std::chrono::seconds(2));

	crashing_thread.join();
}
