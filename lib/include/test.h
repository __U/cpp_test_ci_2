#pragma once

#ifdef SHARED_CPPCRASHWINDOWSLIB
#define CPPCRASHWINDOWSLIB_EXPORT __declspec(dllexport)
#else
#define CPPCRASHWINDOWSLIB_EXPORT __declspec(dllimport)
#endif

class CPPCRASHWINDOWSLIB_EXPORT Test {
public:
    static void foo();
};
