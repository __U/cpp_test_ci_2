#include <iostream>

#ifdef WIN32
#include "client\windows\handler\exception_handler.h"
#else
#include "client/linux/handler/exception_handler.h"
#endif  

#include "test.h"

using namespace std;


#ifdef WIN32
static bool dumpCallback(const wchar_t* dump_path, const wchar_t* minidump_id, void* context, EXCEPTION_POINTERS* exinfo, MDRawAssertionInfo* assertion, bool succeeded) {
	wcout << "dump_path:" << dump_path << " id:" << minidump_id << endl;
	return succeeded;
}
#else
static bool dumpCallback(const google_breakpad::MinidumpDescriptor& descriptor, void* /*context*/, bool succeeded) {
	cout << descriptor.path() << endl;
	return succeeded;
}
#endif    

int main(int argc, char *argv[])
{

#ifdef WIN32

	std::wstring path = L"dumps\\";

	if (CreateDirectoryW(path.c_str(), NULL) || ERROR_ALREADY_EXISTS == GetLastError())
	{
		google_breakpad::ExceptionHandler *pHandler = new google_breakpad::ExceptionHandler(
			path,
			nullptr,
			dumpCallback,
			0,
			google_breakpad::ExceptionHandler::HANDLER_ALL,
			MiniDumpNormal,
			L"",
			0
		);
	}
	else
	{
		cerr << "Failed to create directory." << endl; 
	}	
#else

	google_breakpad::MinidumpDescriptor descriptor("/tmp");
	google_breakpad::ExceptionHandler eh(descriptor, NULL, dumpCallback, NULL, true, -1);
#endif    

    Test::foo();
    return 0;
}
