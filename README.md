#CppCrashWindows
CppCrashWindows is simple application that crashes in the thread running in a dynamically loaded library  
After crash creates dump file near .exe file in 'dump/' directory  

#CI
| Build status          | Systems / Compilers         |
| ------------- | ------------------------------------------ | 
| [![Visual Studio 2017 Builds](https://ci.appveyor.com/api/projects/status/f6knmm5exs2at3vw?svg=true)](https://ci.appveyor.com/project/__U/cpp-test-ci-2)       | Windows (Visual Studio 2017 x86/x64)  |
